package helper

import "golang.org/x/crypto/bcrypt"

func Hash(pass string) (string, error) {
	pwd := []byte(pass)
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.MinCost)
	if err != nil {
		panic(err.Error())
	}
	return string(hash), nil
}

func CheckHashAndPass(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))

	if err != nil {
		return false
	}
	return true
}
