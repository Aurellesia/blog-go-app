module blogGoApp

// +heroku goVersion go1.16

go 1.16

require (
	github.com/kataras/go-sessions/v3 v3.3.0 // indirect
	github.com/lib/pq v1.10.2 // indirect
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97 // indirect
	gorm.io/driver/postgres v1.1.0 // indirect
	gorm.io/gorm v1.21.12 // indirect
)
