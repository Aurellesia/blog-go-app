package main

import (
	"blogGoApp/config"
	"blogGoApp/handler"
	"fmt"
	"net/http"

	_ "github.com/lib/pq"
)

func route() {
	http.HandleFunc("/register", handler.UserRegister)
	http.HandleFunc("/", handler.LandingPage)
	http.HandleFunc("/login", handler.UserLogin)
	http.HandleFunc("/home", handler.Home)
	http.HandleFunc("/logout", handler.UserLogout)
	http.HandleFunc("/createPost", handler.CreatePost)
	http.HandleFunc("/sendCreate", handler.SendCreate)
	http.HandleFunc("/editPost", handler.EditPost)
	http.HandleFunc("/updatePost", handler.UpdatePost)
	http.HandleFunc("/deletePost", handler.DeletePost)
	http.HandleFunc("/detailPost", handler.DetailPost)

	http.HandleFunc("/profile", handler.Profile)
	http.HandleFunc("/editProfile", handler.EditProfile)
	http.HandleFunc("/updateProfile", handler.UpdateProfile)
	http.HandleFunc("/changePicture", handler.ChangePicture)
	http.HandleFunc("/updatePicture", handler.UpdatePicture)

	http.Handle("/files/", http.StripPrefix("/files/", http.FileServer(http.Dir("./files"))))
}

func main() {
	config.Connection()

	route()

	fmt.Println("Server running on port 9090")
	// http.ListenAndServe(":" + os.Getenv("PORT"), nil)
	http.ListenAndServe(":9090", nil)

}
