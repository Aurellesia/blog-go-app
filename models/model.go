package models

import (
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	Username  string
	Password  string
	Email     string
	FirstName string
	LastName  string
	Gender    string
	Phone     string
	Address   string
	Site      string
	Job       string
	Education string
	Workplace string
	Picture   string
	Posts     []Post
}

type Post struct {
	gorm.Model
	Title   string
	Date    string
	Author  string
	Content string
	UserID  uint
}
