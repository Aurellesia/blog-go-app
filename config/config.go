package config

import (
	"fmt"

	"blogGoApp/models"

	_ "github.com/lib/pq"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var DB *gorm.DB

// const (
// 	Host     = "ec2-44-194-145-230.compute-1.amazonaws.com"
// 	Port     = 5432
// 	User     = "isqilmzxeddijr"
// 	Password = "b80ad4306328b5fbaa0f5ccd8b0f5e58f4b249a83c32439f2d76a34ce2de77fc"
// 	DBname   = "dce3tl01i0cvmp"
// 	TimeZone = "Asia/Jakarta"
// )

// func Connection() {
// 	var err error
// 	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
// 		"password=%s dbname=%s sslmode=require TimeZone=%s",
// 		Host, Port, User, Password, DBname, TimeZone)

// 	DB, err = gorm.Open(postgres.Open(psqlInfo), &gorm.Config{})

// 	if err != nil {
// 		panic(err)
// 	}

// 	fmt.Println("Successfully connected!")

// 	DB.Set("gorm:users", "ENGINE=InnoDB").AutoMigrate(&models.User{})
// 	DB.Set("gorm:posts", "ENGINE=InnoDB").AutoMigrate(&models.Post{})

// }

const (
	Host     = "localhost"
	Port     = 5432
	User     = "postgres"
	Password = "12345"
	DBname   = "blog_go"
	TimeZone = "Asia/Jakarta"
)

func Connection() {
	var err error
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable TimeZone=%s",
		Host, Port, User, Password, DBname, TimeZone)

	DB, err = gorm.Open(postgres.Open(psqlInfo), &gorm.Config{})

	if err != nil {
		panic(err)
	}

	fmt.Println("Successfully connected!")

	DB.Set("gorm:users", "ENGINE=InnoDB").AutoMigrate(&models.User{})
	DB.Set("gorm:posts", "ENGINE=InnoDB").AutoMigrate(&models.Post{})

}
