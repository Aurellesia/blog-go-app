package handler

import (
	"blogGoApp/config"
	"blogGoApp/models"
	"net/http"
	"text/template"
)

func LandingPage(w http.ResponseWriter, r *http.Request) {
	var tmpl, err = template.ParseFiles("views/Landing.html")
	// var err error
	if err != nil {
		panic(err.Error())
	}

	var dataposts []models.Post
	rows, err := config.DB.Order("created_at asc").Find(&dataposts).Rows()

	defer rows.Close()

	tmpl.ExecuteTemplate(w, "LANDING", dataposts)
}
