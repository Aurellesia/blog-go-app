package handler

import (
	"blogGoApp/config"
	"blogGoApp/helper"
	"blogGoApp/models"
	"net/http"
	"reflect"
	"text/template"

	"github.com/kataras/go-sessions/v3"
	"golang.org/x/crypto/bcrypt"
)

// var err error

// var SqlDB, er = config.DB.DB()

func CekError(w http.ResponseWriter, r *http.Request, err error) bool {
	// var err error
	if err != nil {
		http.Redirect(w, r, r.Host+r.URL.Path, 301)
		return false
	}
	return true
}

func QueryUser(username string) models.User {
	var users = models.User{}
	config.DB.Find(&users, "username = ?", username)
	return users
}

func Auth(users *models.User, password string, w http.ResponseWriter, r *http.Request) {
	var err error
	err = bcrypt.CompareHashAndPassword([]byte(users.Password), []byte(password))
	if CekError(w, r, err) {
		session := sessions.Start(w, r)
		session.Set("username", users.Username)
		id := int(users.ID)
		// log.Println(reflect.TypeOf(id))
		session.Set("id", id)
		http.Redirect(w, r, "/home", 302)
	}
}

func UserRegister(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.ServeFile(w, r, "views/Register.html")
	}

	username := r.FormValue("username")
	password := r.FormValue("password")
	email := r.FormValue("email")

	users := QueryUser(username)

	if reflect.DeepEqual(models.User{}, users) {
		users.Username = username
		users.Password, _ = helper.Hash(password)
		users.Email = email

		err := config.DB.Create(&users).Error
		if err != nil {
			panic(err.Error)
		}
		http.Redirect(w, r, "/", 301)
	}
}

func UserLogin(w http.ResponseWriter, r *http.Request) {
	var err error
	if r.Method != "POST" {
		http.ServeFile(w, r, "views/Login.html")
		return
	}

	username := r.FormValue("username")
	password := r.FormValue("password")

	users := QueryUser(username)

	if CekError(w, r, err) {
		Auth(&users, password, w, r)
	}
}

func Home(w http.ResponseWriter, r *http.Request) {
	session := sessions.Start(w, r)
	var err error
	if len(session.GetString("username")) != 0 && CekError(w, r, err) {
		var tmpl, er = template.ParseFiles("views/Home.html")

		if er != nil {
			panic(err.Error())
		}

		id, _ := session.GetInt("id")

		posts := ReadPostByUser(id)

		tmpl.ExecuteTemplate(w, "HOME", posts)
	}

	if len(session.GetString("username")) == 0 {
		http.Redirect(w, r, "/", 301)
	}
}

func UserLogout(w http.ResponseWriter, r *http.Request) {
	session := sessions.Start(w, r)
	session.Clear()
	sessions.Destroy(w, r)
	http.Redirect(w, r, "/", 302)
}
