package handler

import (
	"blogGoApp/config"
	"blogGoApp/models"
	"fmt"
	"io/ioutil"
	"net/http"
	"text/template"

	"github.com/kataras/go-sessions/v3"
)

func Profile(w http.ResponseWriter, r *http.Request) {
	session := sessions.Start(w, r)
	var tmpl, err = template.ParseFiles("views/Profile.html")
	if err != nil {
		panic(err.Error())
	}

	id, _ := session.GetInt("id")
	dataUser := ReadProfile(id)
	tmpl.ExecuteTemplate(w, "PROFILE", dataUser)

}

func ReadProfile(id int) models.User {
	var dataUser models.User

	err := config.DB.Where("id = ?", id).Find(&dataUser).Select("username", "email", "picture").Error

	if err != nil {
		panic(err.Error())
	}

	return dataUser
}

func EditProfile(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		session := sessions.Start(w, r)
		var tmpl, err = template.ParseFiles("views/EditProfile.html")
		var user models.User

		if err != nil {
			panic(err.Error())
		}

		id, _ := session.GetInt("id")
		result := config.DB.First(&user, id)
		result.Scan(&user)

		tmpl.ExecuteTemplate(w, "EDIT-PROFILE", user)
	}
}

func UpdateProfile(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		var user models.User
		id := r.FormValue("id")
		username := r.FormValue("username")
		email := r.FormValue("email")
		firstName := r.FormValue("first_name")
		lastName := r.FormValue("last_name")
		gender := r.FormValue("gender")
		phone := r.FormValue("phone")
		address := r.FormValue("address")
		site := r.FormValue("site")
		job := r.FormValue("job")
		education := r.FormValue("education")
		workplace := r.FormValue("workplace")

		err := config.DB.Model(&user).Where("id = ?", id).Select("username", "email",
			"first_name", "last_name", "gender", "phone", "address", "site", "job",
			"education", "workplace").Updates(models.User{Username: username, Email: email, FirstName: firstName, LastName: lastName, Gender: gender, Phone: phone,
			Address: address, Site: site, Job: job, Education: education, Workplace: workplace}).Error

		if err != nil {
			panic(err.Error())
		}

		http.Redirect(w, r, "/profile", 301)
	}
}

func ChangePicture(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		session := sessions.Start(w, r)
		var tmpl, err = template.ParseFiles("views/ChangePicture.html")
		var user models.User

		if err != nil {
			panic(err.Error())
		}

		id, _ := session.GetInt("id")
		result := config.DB.First(&user, id)
		result.Scan(&user)

		tmpl.ExecuteTemplate(w, "CHANGE-PICTURE", user)
	}
}

func UpdatePicture(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		var user models.User
		id := r.FormValue("id")
		// UPLOAD FILE
		r.ParseMultipartForm(1024)
		file, hand, err := r.FormFile("file")
		if err != nil {
			panic(err.Error())
		}
		defer file.Close()
		fmt.Printf("Uploaded file : %+v\n", hand.Filename)
		fmt.Printf("File size : %+v\n", hand.Size)

		tempFile, err := ioutil.TempFile("files", "upload-*.png")
		if err != nil {
			fmt.Println(err)
		}
		defer tempFile.Close()

		fileBytes, err := ioutil.ReadAll(file)
		if err != nil {
			fmt.Println(err)
		}
		tempFile.Write(fileBytes)
		fmt.Println("Successfully uploaded file")

		picture := tempFile.Name()

		err = config.DB.Model(&user).Where("id = ?", id).Select("picture").Updates(models.User{Picture: picture}).Error

		if err != nil {
			panic(err.Error())
		}

		http.Redirect(w, r, "/profile", 301)
	}
}
