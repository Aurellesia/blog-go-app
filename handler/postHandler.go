package handler

import (
	"blogGoApp/config"
	"blogGoApp/models"
	"net/http"
	"text/template"
	"time"

	"github.com/kataras/go-sessions/v3"
)

func ReadPostByUser(id int) []models.Post {
	var dataPosts []models.Post

	rows, err := config.DB.Where("user_id = ?", id).Find(&dataPosts).Select("id", "title", "date", "author", "content").Rows()
	if err != nil {
		panic(err.Error())
	}

	defer rows.Close()

	return dataPosts
}

func DetailPost(w http.ResponseWriter, r *http.Request) {
	var tmpl, err = template.ParseFiles("views/Detail.html")
	var datapost models.Post

	if err != nil {
		panic(err.Error())
	}

	id := r.URL.Query().Get("id")
	result := config.DB.First(&datapost, id)
	result.Scan(&datapost)

	tmpl.ExecuteTemplate(w, "DETAIL", datapost)
}

func SendCreate(w http.ResponseWriter, r *http.Request) {
	session := sessions.Start(w, r)
	title := r.FormValue("title")
	content := r.FormValue("content")
	jkt, _ := time.LoadLocation("Asia/Jakarta")
	date := time.Now().In(jkt).Format("2006-01-02 15:04:05")

	author := session.GetString("username")
	id, _ := session.GetInt("id")

	var post models.Post
	if (models.Post{}) == post {
		post.Title = title
		post.Author = author
		post.Content = content
		post.Date = date
		post.UserID = uint(id)

		err := config.DB.Create(&post).Error
		if err != nil {
			panic(err.Error())
		}
		http.Redirect(w, r, "/home", 301)
	}
}

func CreatePost(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.ServeFile(w, r, "views/CreatePost.html")
	}
}

func EditPost(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		var tmpl, err = template.ParseFiles("views/Edit.html")
		var dataPost models.Post

		if err != nil {
			panic(err.Error())
		}

		id := r.URL.Query().Get("id")

		result := config.DB.First(&dataPost, id)
		result.Scan(&dataPost)

		tmpl.ExecuteTemplate(w, "EDIT", dataPost)
	}

}

func UpdatePost(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		var datapost models.Post
		title := r.FormValue("title")
		content := r.FormValue("content")
		id := r.FormValue("id")

		err := config.DB.Model(&datapost).Where("id = ?", id).Select("title", "content").Updates(models.Post{Title: title, Content: content}).Error

		if err != nil {
			panic(err.Error())
		}
		http.Redirect(w, r, "/home", 301)
	}
}

func DeletePost(w http.ResponseWriter, r *http.Request) {
	var datapost models.Post
	id := r.URL.Query().Get("id")
	err := config.DB.Where("id = ?", id).Delete(&datapost).Error
	if err != nil {
		panic(err.Error)
	}
	http.Redirect(w, r, "/home", 301)
}
